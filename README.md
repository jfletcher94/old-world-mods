#### My Modding Guides
- [Transpiler Patches](https://gitlab.com/jfletcher94/old-world-mods/-/wikis/home)
- [Custom XML Mods](https://gitlab.com/jfletcher94/ow_customxmltags/-/wikis/home)

#### My Old World Mods
- [Desert Roads](https://gitlab.com/jfletcher94/ow_desertroads) ([Mod.io](https://mod.io/g/oldworld/m/desert-roads)) ([Steam Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=2985482490))
- [Stockpile Limits](https://gitlab.com/jfletcher94/ow_stockpilelimits) ([Mod.io](https://mod.io/g/oldworld/m/stockpile-limits)) ([Steam Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=2983777103))
- [Bonus Unit Family](https://gitlab.com/jfletcher94/ow_bonusunitfamily) ([Mod.io](https://mod.io/g/oldworld/m/bonus-unit-family)) ([Steam Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=2936489743))
- [Faster Fortify](https://gitlab.com/jfletcher94/ow_fasterfortify) ([Mod.io](https://mod.io/g/oldworld/m/faster-fortify1)) ([Steam Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=2931605448))
- [Custom XML Tags](https://gitlab.com/jfletcher94/ow_customxmltags) ([Mod.io](https://mod.io/g/oldworld/m/custom-xml-tags)) ([Steam Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=2964014443))

#### Also
- Find me on [Discord](https://discord.com/users/777563893677162527) on the [Old World](https://discord.gg/AfDc28y3cT) server
- My [Mod.io](https://mod.io/g/oldworld/u/thremtopod) page
- My [Steam Workshop](https://steamcommunity.com/id/DiogenesBarrel/myworkshopfiles/?appid=597180) page
